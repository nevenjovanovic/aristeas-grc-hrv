9-11

Pošto je postavljen na čelo kraljeve knjižnice, Demetrije Falerski dobio je znatna sredstva kako bi skupio, po mogućnosti, sve knjige naseljenog svijeta, te je kupovanjem i prepisivanjem ostvario, koliko je bio u stanju, kraljev nalog.

Tako je, u našoj prisutnosti, na pitanje "Koliko tisuća knjiga ima?" odgovorio: "Preko dvjesto tisuća, kralju; a za kratko ću vrijeme pribaviti ostatak, kako bi se dostigao broj od petsto tisuća. Nego, dobio sam vijest da su i zakoni Židova vrijedni prepisivanja i ove knjižnice koju imaš."

Kralj odvrati: "Pa što te sprečava da ih pribaviš? Za tu ti je svrhu sve na raspolaganju." A Demetrije reče: "Potreban je prijevod; kod Židova je u upotrebi posebno pismo, isto kao što Egipćani imaju vlastiti način pisanja slova, a i poseban jezik. Smatra se da se služe sirijskim, ali nije tako; govor je drugačiji." Obaviješten o svemu, kralj naredi da se uputi pismo velikom svećeniku Židova, kako bi se ostvarilo rečeno.


121-122

(Veliki jeruzalemski svećenik Eleazar) odabrao je najbolje ljude, koji su se isticali obrazovanjem, budući da su potjecali od uglednih roditelja; ti ljudi nisu ovladali samo judejskim, nego su marljivo proučili i grčki; stoga su bili odličan izbor za poslanstva i u tome bi se, kad god je trebalo, pokazali odličnima; a imali su i silan dar za rasprave i pitanja o zakonu, pri čemu je njihov stav bio uravnotežen (a to je najljepše); oslobodili su se grubog i barbarskog načina razmišljanja, a istovremeno su nadvladali i samozadovoljstvo i stav da su bolji od drugih, te su u raspravama znali i slušati i odgovoriti na svako pitanje, i svi su njegovali te kvalitete, i trudili se u njima nadmašiti jedan drugoga; tako su svi bili dostojni svoga vođe i njegove vrline.

308-311

Kad je prevođenje bilo okončano, Demetrije je okupio judejsku zajednicu na mjesto gdje se prijevod i pripremao, i pred svima ga pročitao; bili su prisutni i prevodioci, kojima je zajednica izrazila silnu zahvalnost, jer su joj priskrbili silno dobro. Jednako su tako pozdravili i Demetrija, i zamolili ga da njihovim glavarima preda prijepis čitavoga zakona. Pošto su svici pročitani, nastupiše zajedno svećenici, starješine prevodilaca, građanski predstavnici i vođe zajednice i rekoše: "Budući da je prevedeno lijepo, pobožno i u svakom pogledu točno, u redu je da djelo ostano ovakvo kakvo jest, i da ne dođe ni do kakve prerade." Svi su se suglasili s tim riječima, te su naredili da, kao što je kod njih običaj, proklet bude onaj tko učini neku preradu, bilo dodajući, ili premještajući nešto od zapisane cjeline, ili oduzimajući; valjano su to učinili, kako bi prijevod ostao zauvijek sačuvan u neizmijenjenom stanju.


