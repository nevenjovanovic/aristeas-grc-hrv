# README #

A Greek text and Croatian translation of the Letter of Aristeas.

This directory was prepared by [Neven Jovanović](http://orcid.org/0000-0002-9119-399X).

# License #

[CC Attribution 4.0 International](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/LICENSE.md)

# Who do I talk to? #

* Repo owner: [Neven Jovanović](https://bitbucket.org/nevenjovanovic)

